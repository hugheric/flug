{
    "id": "cf18d3f6-2044-418d-9d88-c8b2bfd32ac6",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Yu Gothic",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c2fbcadf-6fae-4d6b-ae23-7c82687f67fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 427,
                "offset": 0,
                "shift": 76,
                "w": 76,
                "x": 3220,
                "y": 860
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c666b33a-24b4-43e5-85f5-58b1b45aaa31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 427,
                "offset": 27,
                "shift": 96,
                "w": 42,
                "x": 3704,
                "y": 860
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "53358ab5-1df4-4d96-ba4b-dad71d7d58a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 427,
                "offset": 15,
                "shift": 118,
                "w": 88,
                "x": 2714,
                "y": 860
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "dc4a4fcc-15b0-431e-9e95-6bbb83fab853",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 427,
                "offset": 9,
                "shift": 166,
                "w": 148,
                "x": 1420,
                "y": 431
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3f33c1d2-9b87-4514-b13f-4e465c681c12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 427,
                "offset": 12,
                "shift": 166,
                "w": 141,
                "x": 1720,
                "y": 431
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "48935e00-f6ef-4e3a-9d2d-6ddc8eba8d1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 427,
                "offset": 13,
                "shift": 225,
                "w": 199,
                "x": 1076,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c272db98-600e-45dc-8d5c-4d45fa2ca50d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 427,
                "offset": 13,
                "shift": 203,
                "w": 184,
                "x": 1277,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "054ee87e-7efb-4614-a5f7-46704faaf716",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 427,
                "offset": 15,
                "shift": 64,
                "w": 34,
                "x": 4028,
                "y": 860
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "90101ce0-e4b9-490b-9cf4-3d5ebd2d34b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 427,
                "offset": 30,
                "shift": 107,
                "w": 72,
                "x": 3298,
                "y": 860
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2f1650fa-8553-4a63-a3e3-1859b4253b72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 427,
                "offset": 5,
                "shift": 107,
                "w": 72,
                "x": 3372,
                "y": 860
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8e535d32-8151-4de5-82e4-7427318451f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 427,
                "offset": 12,
                "shift": 135,
                "w": 111,
                "x": 2414,
                "y": 860
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0c95577b-869a-44e8-8179-e213766c4b3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 427,
                "offset": 17,
                "shift": 198,
                "w": 164,
                "x": 3035,
                "y": 2
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b7003224-ce2f-4239-8cd6-7277a5cbb2db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 427,
                "offset": 18,
                "shift": 74,
                "w": 39,
                "x": 3748,
                "y": 860
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9d368ade-ddd2-4eba-865e-7ad345898cff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 427,
                "offset": 18,
                "shift": 115,
                "w": 79,
                "x": 3059,
                "y": 860
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7de495cc-1809-4b79-9b60-e211535ed043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 427,
                "offset": 18,
                "shift": 74,
                "w": 39,
                "x": 3789,
                "y": 860
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9f25b777-2fdd-4523-9bd3-e85dc360bc8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 427,
                "offset": 4,
                "shift": 134,
                "w": 126,
                "x": 1176,
                "y": 860
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2df8cd87-3be6-4f6c-8d5d-5e1fa291c730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 427,
                "offset": 12,
                "shift": 153,
                "w": 129,
                "x": 397,
                "y": 860
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "69e91326-a29e-4155-a510-4676b20b915b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 427,
                "offset": 21,
                "shift": 153,
                "w": 114,
                "x": 2183,
                "y": 860
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e0c63d50-a59c-4c71-b00a-b16990b637ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 427,
                "offset": 12,
                "shift": 153,
                "w": 127,
                "x": 1047,
                "y": 860
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "eeefca85-1997-4f19-9677-0fb133ba33ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 427,
                "offset": 9,
                "shift": 153,
                "w": 131,
                "x": 3773,
                "y": 431
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3590c212-5713-4706-b84a-7ba1a9d2f9b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 427,
                "offset": 9,
                "shift": 153,
                "w": 135,
                "x": 2288,
                "y": 431
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f3a203f3-8eee-4ab5-8dcc-9246e52538fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 427,
                "offset": 12,
                "shift": 153,
                "w": 128,
                "x": 658,
                "y": 860
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ffe063ae-0fd1-49cc-98d9-1e026adea99b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 427,
                "offset": 13,
                "shift": 153,
                "w": 129,
                "x": 266,
                "y": 860
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ad907739-7861-4ee4-8c25-b6d76d0bf26b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 427,
                "offset": 13,
                "shift": 153,
                "w": 125,
                "x": 1432,
                "y": 860
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "269668ef-8959-45e3-ae97-5e586f57a4a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 427,
                "offset": 10,
                "shift": 153,
                "w": 133,
                "x": 2969,
                "y": 431
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a4b781c1-7c5f-4066-8321-c8ab1f972e9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 427,
                "offset": 12,
                "shift": 153,
                "w": 128,
                "x": 788,
                "y": 860
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "68780a4b-ce75-40cb-b5fb-613aae41b75a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 427,
                "offset": 21,
                "shift": 81,
                "w": 39,
                "x": 3871,
                "y": 860
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "3d75a37d-7b1d-47ef-89d3-c890b753f1ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 427,
                "offset": 21,
                "shift": 81,
                "w": 39,
                "x": 3830,
                "y": 860
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d40b4831-1a8e-474e-b263-822ff5497b61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 427,
                "offset": 17,
                "shift": 198,
                "w": 164,
                "x": 3533,
                "y": 2
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "fa5bd624-f3da-47ca-9f6e-0426ccababba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 427,
                "offset": 17,
                "shift": 198,
                "w": 164,
                "x": 3201,
                "y": 2
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "28204682-85f6-4e25-9e37-4b01906d7ccc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 427,
                "offset": 17,
                "shift": 198,
                "w": 164,
                "x": 3367,
                "y": 2
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ca22b3c7-27eb-4aae-b979-6c40e00e89a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 427,
                "offset": 9,
                "shift": 152,
                "w": 132,
                "x": 3372,
                "y": 431
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0b2112e4-fafd-4107-9f39-f59db828281c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 427,
                "offset": 13,
                "shift": 237,
                "w": 211,
                "x": 451,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f4e5583a-8941-49b3-88d6-22d52f5b5ba1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 427,
                "offset": 6,
                "shift": 183,
                "w": 172,
                "x": 1823,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a493ca33-7c40-4a24-87b1-979310fabf9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 427,
                "offset": 24,
                "shift": 189,
                "w": 152,
                "x": 1113,
                "y": 431
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "938f06d8-a819-4170-ba9a-d919f5aebf0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 427,
                "offset": 13,
                "shift": 192,
                "w": 171,
                "x": 2691,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4531d433-cdfc-4508-8bff-d3f2bdedd652",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 427,
                "offset": 24,
                "shift": 206,
                "w": 169,
                "x": 2864,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "62c9ca57-3d53-4cc9-bdd0-bab80e123139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 427,
                "offset": 24,
                "shift": 173,
                "w": 140,
                "x": 2006,
                "y": 431
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "0faf95ea-9774-40d9-a3e1-35c632bfbd76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 427,
                "offset": 24,
                "shift": 169,
                "w": 138,
                "x": 2148,
                "y": 431
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4d7529dc-59a3-457a-b8cd-6c7a54713f6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 427,
                "offset": 13,
                "shift": 204,
                "w": 171,
                "x": 2518,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d33bdc9b-eb4d-4d12-8b02-4ef7cec8f7d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 427,
                "offset": 24,
                "shift": 207,
                "w": 159,
                "x": 164,
                "y": 431
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3b6a8fc3-2c2a-42e9-91ea-4dc1eda0fb99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 427,
                "offset": 24,
                "shift": 86,
                "w": 38,
                "x": 3912,
                "y": 860
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f43ff4fe-ecaa-4c2e-a6bf-c02c3d664a2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 427,
                "offset": 5,
                "shift": 113,
                "w": 84,
                "x": 2804,
                "y": 860
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "91461bb0-002a-43d9-8868-64c01baa70ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 427,
                "offset": 24,
                "shift": 191,
                "w": 162,
                "x": 3699,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "635c8d97-1f0f-4c4a-afa0-32b2161a3644",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 427,
                "offset": 24,
                "shift": 167,
                "w": 135,
                "x": 2425,
                "y": 431
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6b1e7313-16e8-4fad-ab38-d877a2998bef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 427,
                "offset": 24,
                "shift": 254,
                "w": 206,
                "x": 664,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a2e1b3b1-4764-4ede-a19f-0186679940ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 427,
                "offset": 24,
                "shift": 208,
                "w": 160,
                "x": 2,
                "y": 431
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d2f90420-ac6f-43dd-9d97-ccf34b03fbfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 427,
                "offset": 13,
                "shift": 204,
                "w": 178,
                "x": 1463,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4d608cba-7312-48f7-99ae-6ce83bd30136",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 427,
                "offset": 24,
                "shift": 185,
                "w": 148,
                "x": 1570,
                "y": 431
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6cb483b2-588f-4ecd-839c-0b5fa299c8c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 427,
                "offset": 13,
                "shift": 204,
                "w": 178,
                "x": 1643,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "f2529d16-b75e-4a42-8597-dd336118da55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 427,
                "offset": 24,
                "shift": 187,
                "w": 156,
                "x": 645,
                "y": 431
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "05b7de52-6315-4fc8-8c4d-d22b60976fc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 427,
                "offset": 9,
                "shift": 176,
                "w": 154,
                "x": 803,
                "y": 431
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9c067bd9-bb6c-4a47-8e87-0cb4bf00bab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 427,
                "offset": 8,
                "shift": 176,
                "w": 160,
                "x": 3863,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "652ff2d1-39c3-4416-81ff-101c75444941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 427,
                "offset": 24,
                "shift": 206,
                "w": 158,
                "x": 325,
                "y": 431
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ef574adb-59f8-43d8-b32c-6b5c3639bb29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 427,
                "offset": 6,
                "shift": 183,
                "w": 172,
                "x": 2171,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "79af045b-bbd9-4355-a5ff-d75a7a6caf23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 427,
                "offset": 6,
                "shift": 244,
                "w": 232,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5f5f74e3-5b7f-478d-bffb-bfc2a0628c10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 427,
                "offset": 6,
                "shift": 183,
                "w": 171,
                "x": 2345,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f1c3b0c8-4eb4-4504-8f01-3d75242f187c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 427,
                "offset": 6,
                "shift": 183,
                "w": 172,
                "x": 1997,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d6a96c1a-4d31-4f82-9ddb-437cf21c5285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 427,
                "offset": 10,
                "shift": 171,
                "w": 152,
                "x": 959,
                "y": 431
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4464bb70-9437-4b29-9020-1d81a881f76c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 427,
                "offset": 33,
                "shift": 108,
                "w": 68,
                "x": 3516,
                "y": 860
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "82327185-85a2-4891-94c4-b4f1e0bfb676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 427,
                "offset": 6,
                "shift": 164,
                "w": 151,
                "x": 1267,
                "y": 431
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "72941bc8-20fb-4e40-ad3d-bd5a67bd3dcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 427,
                "offset": 7,
                "shift": 108,
                "w": 68,
                "x": 3446,
                "y": 860
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9922f020-5827-4469-bb87-b2b1b9cafe90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 427,
                "offset": 7,
                "shift": 134,
                "w": 120,
                "x": 2061,
                "y": 860
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4c9a0793-1504-4423-8956-7e43b47c4366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 427,
                "offset": 0,
                "shift": 134,
                "w": 134,
                "x": 2562,
                "y": 431
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "464894c7-3ce7-4509-85f7-8e68a8bf2f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 427,
                "offset": 26,
                "shift": 134,
                "w": 52,
                "x": 3650,
                "y": 860
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c3939754-bd3c-488f-9b40-10adffba62ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 427,
                "offset": 12,
                "shift": 155,
                "w": 126,
                "x": 1304,
                "y": 860
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "70c6aa23-405a-419d-89f7-8e0870772a8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 427,
                "offset": 21,
                "shift": 164,
                "w": 130,
                "x": 2,
                "y": 860
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "98cf8be0-30b1-493f-8aa3-457f93fca45e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 427,
                "offset": 12,
                "shift": 147,
                "w": 128,
                "x": 528,
                "y": 860
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0dae57e4-f186-4743-afc7-7612a01faf76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 427,
                "offset": 12,
                "shift": 164,
                "w": 131,
                "x": 3906,
                "y": 431
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "478dfa71-cef7-4c5a-89bc-5f218e38a001",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 427,
                "offset": 12,
                "shift": 157,
                "w": 133,
                "x": 2834,
                "y": 431
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8ce6274b-3d91-4640-b356-3d1c1faac8cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 427,
                "offset": 6,
                "shift": 100,
                "w": 92,
                "x": 2527,
                "y": 860
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e3486c18-d497-4e5e-9fc4-ef4d1611576a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 427,
                "offset": 8,
                "shift": 155,
                "w": 141,
                "x": 1863,
                "y": 431
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c93405bc-b326-4b15-9514-95b7f6496e58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 427,
                "offset": 21,
                "shift": 165,
                "w": 124,
                "x": 1559,
                "y": 860
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e5bd236b-606c-4d7d-bd86-052f16927874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 427,
                "offset": 20,
                "shift": 76,
                "w": 37,
                "x": 3952,
                "y": 860
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "281a5442-d067-402b-8e06-d1b0a4597803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 427,
                "offset": 0,
                "shift": 83,
                "w": 62,
                "x": 3586,
                "y": 860
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "626e29cc-afaf-4b2b-a714-ff12269ddc9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 427,
                "offset": 21,
                "shift": 152,
                "w": 127,
                "x": 918,
                "y": 860
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7dce4b20-c258-4fa3-acd5-abac82db8daa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 427,
                "offset": 21,
                "shift": 77,
                "w": 35,
                "x": 3991,
                "y": 860
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "576029bd-790f-4733-ac8a-a69bd4058a7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 427,
                "offset": 21,
                "shift": 243,
                "w": 202,
                "x": 872,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "45b1b072-82af-4d8b-ae2a-738d2499d425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 427,
                "offset": 21,
                "shift": 165,
                "w": 124,
                "x": 1811,
                "y": 860
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "8f8a91e4-7b82-4528-a848-4c792df204a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 427,
                "offset": 12,
                "shift": 158,
                "w": 134,
                "x": 2698,
                "y": 431
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "cf996f8c-0f16-46b0-8cd8-56d82ab27c1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 427,
                "offset": 21,
                "shift": 164,
                "w": 130,
                "x": 134,
                "y": 860
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "18d65f39-528b-4c5f-bdb0-a4690956f595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 427,
                "offset": 12,
                "shift": 164,
                "w": 131,
                "x": 3640,
                "y": 431
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "71d2a928-5f8f-49ff-96b0-12f67bf97956",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 427,
                "offset": 21,
                "shift": 105,
                "w": 78,
                "x": 3140,
                "y": 860
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "42037c26-b003-4661-b23c-cc16e63512f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 427,
                "offset": 9,
                "shift": 142,
                "w": 122,
                "x": 1937,
                "y": 860
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e864de02-25ac-42b2-a4c0-380022f75ff8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 427,
                "offset": 6,
                "shift": 101,
                "w": 91,
                "x": 2621,
                "y": 860
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5d01d599-dee9-46c0-a223-3473b5904e05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 427,
                "offset": 20,
                "shift": 165,
                "w": 124,
                "x": 1685,
                "y": 860
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "58a1f2e5-bcb3-4e39-a4be-7914582b96f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 427,
                "offset": 5,
                "shift": 142,
                "w": 132,
                "x": 3104,
                "y": 431
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "921606d2-6842-45a1-ab27-1fee06045d65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 427,
                "offset": 5,
                "shift": 223,
                "w": 213,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "eebe4fdb-1878-4833-b4ef-c07ab17bfec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 427,
                "offset": 5,
                "shift": 142,
                "w": 132,
                "x": 3238,
                "y": 431
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "d82916ae-c667-46d4-998d-42099d8c5691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 427,
                "offset": 5,
                "shift": 142,
                "w": 132,
                "x": 3506,
                "y": 431
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "362a49c8-49cd-4c20-9265-cd550608b24b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 427,
                "offset": 8,
                "shift": 129,
                "w": 113,
                "x": 2299,
                "y": 860
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "bd7f3d8b-e528-4edc-9fd9-abb372338a00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 427,
                "offset": 18,
                "shift": 108,
                "w": 83,
                "x": 2890,
                "y": 860
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "daf12279-c3a5-4188-941b-36330eb8d81c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 427,
                "offset": 46,
                "shift": 108,
                "w": 16,
                "x": 4064,
                "y": 860
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2b62af1e-4faa-46c0-b6f5-7949ceb9a5a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 427,
                "offset": 7,
                "shift": 108,
                "w": 82,
                "x": 2975,
                "y": 860
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b0013955-84b6-472b-aca4-1a5198d3d9f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 427,
                "offset": 17,
                "shift": 192,
                "w": 158,
                "x": 485,
                "y": 431
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 200,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}