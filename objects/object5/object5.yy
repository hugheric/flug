{
    "id": "e71de20d-c901-45e5-9a68-8df0c68d94bf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object5",
    "eventList": [
        {
            "id": "70fa95be-0c53-465d-8f5a-c99347da9f39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e71de20d-c901-45e5-9a68-8df0c68d94bf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d384bc72-bc54-47d1-9dff-08894ac4da37",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4b7f01f4-0ee7-45f3-9ad8-8908fe4955ca",
    "visible": true
}