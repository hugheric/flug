event_inherited()

if clicked && moveable {
	obj_textbox.charCount = 0
	obj_textbox.page = 7	
}

if !clicked && moveable{
	script_execute(scr_placement)
	
	if x < 1160 && x > 950 {
		if y < 2630 && y > 2450 {
			if image_angle < 1 || image_angle > 340 {
				audio_play_sound(snd_ding, 2, 0);
				moveable = false;
				x = 1060
				y = 2545
				image_angle = -9
				global.peices += 1
			}
		}
	}
}