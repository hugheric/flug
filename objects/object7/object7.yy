{
    "id": "935f27cf-a073-435a-a914-7c7a1e440d0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object7",
    "eventList": [
        {
            "id": "a9ac224d-6036-408c-8e61-904137f53c6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "935f27cf-a073-435a-a914-7c7a1e440d0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d384bc72-bc54-47d1-9dff-08894ac4da37",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "23c4dae9-2655-4912-a147-d5871e993488",
    "visible": true
}