event_inherited()

if clicked && moveable {
	obj_textbox.charCount = 0
	obj_textbox.page = 5	
}

if !clicked && moveable{
	script_execute(scr_placement)
	
	if x < 3000 && x > 2500 {
		if y < 3100 && y > 2800 {
			if image_angle < 11 || image_angle > 350 {
				audio_play_sound(snd_ding, 2, 0);
				moveable = false;
				x = 2825
				y = 2930
				image_angle = 3
				global.peices += 1
			}
		}
	}
}