{
    "id": "032294f1-a1c2-47bf-b59f-b722dcadbdc8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_noflug",
    "eventList": [
        {
            "id": "4569cb14-8f82-42a6-a787-d09844dfdf2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "032294f1-a1c2-47bf-b59f-b722dcadbdc8"
        },
        {
            "id": "e7c0c21e-a116-434e-a043-3ef43de84d1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "032294f1-a1c2-47bf-b59f-b722dcadbdc8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4499f179-72b6-4d5e-b463-3e1203bd29c6",
    "visible": true
}