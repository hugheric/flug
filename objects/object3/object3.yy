{
    "id": "18692622-5833-44f7-a0cf-4f91f8ce6d45",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object3",
    "eventList": [
        {
            "id": "a5ae499c-8416-43f8-837f-1420f90b752a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "18692622-5833-44f7-a0cf-4f91f8ce6d45"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d384bc72-bc54-47d1-9dff-08894ac4da37",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "59bb68b8-10df-4930-bc68-c88971286e59",
    "visible": true
}