{
    "id": "c102d80d-2865-47d0-91af-9fcfa5f96af7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_info",
    "eventList": [
        {
            "id": "aeddb1cf-0f32-42d3-8a71-fcf607d6197f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c102d80d-2865-47d0-91af-9fcfa5f96af7"
        },
        {
            "id": "d0ab80da-1d52-4216-a290-5ca40ee28721",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 18,
            "eventtype": 9,
            "m_owner": "c102d80d-2865-47d0-91af-9fcfa5f96af7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e9de17ff-e8d9-4af5-9621-acd019b79f02",
    "visible": true
}