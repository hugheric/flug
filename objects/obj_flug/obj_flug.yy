{
    "id": "b4aee1c9-15a0-4099-86cb-d363e3148c2c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_flug",
    "eventList": [
        {
            "id": "e6e95c38-ae2b-4dd3-aad6-8ddad81a24f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b4aee1c9-15a0-4099-86cb-d363e3148c2c"
        },
        {
            "id": "d845c2f7-95c1-4421-8ac9-559a747e2308",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b4aee1c9-15a0-4099-86cb-d363e3148c2c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4f3ecc62-5c3b-43b7-b4ea-fa018cd2edbe",
    "visible": true
}