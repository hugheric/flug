if (clicked && moveable) {
    x = mouse_x
	y = mouse_y
	
	if keyboard_check(ord("A")){
		image_angle = image_angle + 3	
	}
	
	if keyboard_check(ord("D")){
		image_angle = image_angle - 3	
	}
}

if image_angle >= 360 {
	image_angle = image_angle - 360;	
} else if image_angle < 0 {
	image_angle = 360 - abs(image_angle);
}