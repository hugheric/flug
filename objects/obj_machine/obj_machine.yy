{
    "id": "bb45e61c-7e07-4a81-a3a7-5f051eec8fce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_machine",
    "eventList": [
        {
            "id": "503f33eb-ffd3-4f11-aaf2-c276005b1778",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bb45e61c-7e07-4a81-a3a7-5f051eec8fce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "52bce536-eed1-485b-b40c-446b04984e9d",
    "visible": true
}