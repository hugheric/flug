event_inherited()

if clicked && moveable {
	obj_textbox.charCount = 0
	obj_textbox.page = 6	
}

if !clicked && moveable{
	script_execute(scr_placement)
	
	if x < 4080 && x > 3880 {
		if y < 2180 && y > 1980 {
			if image_angle < 10 || image_angle > 350 {
				audio_play_sound(snd_ding, 2, 0);
				moveable = false;
				x = 3980
				y = 2080
				image_angle = 0
				global.peices += 1
			}
		}
	}
}