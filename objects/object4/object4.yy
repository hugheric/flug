{
    "id": "23c2d6b3-19c7-422d-8931-82f2954658fe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object4",
    "eventList": [
        {
            "id": "96b13f31-32e5-4fc3-8b62-ce9e8ac6046d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "23c2d6b3-19c7-422d-8931-82f2954658fe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d384bc72-bc54-47d1-9dff-08894ac4da37",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7328bec5-b59c-49bb-9e9d-954fdaeaac69",
    "visible": true
}