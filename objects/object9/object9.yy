{
    "id": "bd903d82-af51-4ee9-ac1d-8f71cf7a9dfa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object9",
    "eventList": [
        {
            "id": "415cceae-103e-4bb7-a3c7-6df5735e08a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "bd903d82-af51-4ee9-ac1d-8f71cf7a9dfa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d384bc72-bc54-47d1-9dff-08894ac4da37",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7caf7c00-f6ce-4f92-b35f-b90e1e464750",
    "visible": true
}