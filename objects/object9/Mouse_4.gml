event_inherited()

if clicked && moveable {
	obj_textbox.charCount = 0
	obj_textbox.page = 10
}

if !clicked && moveable{
	script_execute(scr_placement)
	
	if x < 2285 && x > 2085 {
		if y < 3105 && y > 2905 {
			if image_angle < 10 || image_angle > 350 {
				audio_play_sound(snd_ding, 2, 0);
				moveable = false;
				x = 2185
				y = 3005
				image_angle = 0
				global.peices += 1
			}
		}
	}
}