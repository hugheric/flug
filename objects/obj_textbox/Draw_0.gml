draw_self()

if charCount < string_length(text[page]){
	obj_boss.image_speed = 1;
	charCount +=1;
	if !audio_is_playing(snd_alien){
		audio_sound_pitch(snd_alien, random_range(.8,2))
		audio_play_sound(snd_alien, 2, 0);	
	}
} else {
	obj_boss.image_speed = 0;
}
textpart = string_copy(text[page],1,charCount);
draw_text_ext_transformed(x,y,textpart,-1,250 ,6 ,7 ,0 );