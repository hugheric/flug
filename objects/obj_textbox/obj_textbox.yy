{
    "id": "5380e68d-6c60-49f6-a795-43bacf691652",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_textbox",
    "eventList": [
        {
            "id": "736d8397-3891-44dc-97ff-2c96363bf771",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5380e68d-6c60-49f6-a795-43bacf691652"
        },
        {
            "id": "909095f2-a6d5-4d58-96ed-29c53013b8df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5380e68d-6c60-49f6-a795-43bacf691652"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "39a5ec08-1aaa-495a-b7f8-75034befd555",
    "visible": true
}