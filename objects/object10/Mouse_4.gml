event_inherited()

if clicked && moveable {
	obj_textbox.charCount = 0
	obj_textbox.page = 8	
}

if !clicked && moveable{
	script_execute(scr_placement)
	
	if x < 2185 && x > 1985 {
		if y < 3230 && y > 3050 {
			if image_angle < 16 || image_angle > 360 {
				audio_play_sound(snd_ding, 2, 0);
				moveable = false;
				x = 2085
				y = 3130
				image_angle = 6
				global.peices += 1
			}
		}
	}
}