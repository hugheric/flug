{
    "id": "49f47193-d631-4cef-845b-f6aa5f18ce4c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object10",
    "eventList": [
        {
            "id": "bf43c5d6-2a09-4dd3-af42-eab4248f30db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "49f47193-d631-4cef-845b-f6aa5f18ce4c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d384bc72-bc54-47d1-9dff-08894ac4da37",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75db853d-3875-4eff-a28e-f4dfe64d633c",
    "visible": true
}