{
    "id": "0f2035e3-16a5-48b0-b4a4-21aae6ede753",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_nextbutton",
    "eventList": [
        {
            "id": "a168b1b8-7eb7-4cdc-b35c-92782edd8542",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0f2035e3-16a5-48b0-b4a4-21aae6ede753"
        },
        {
            "id": "31ad79ad-8322-466f-b115-bf9128085f14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0f2035e3-16a5-48b0-b4a4-21aae6ede753"
        },
        {
            "id": "fbd88f6c-4a46-4dac-90ab-e44079f0c452",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0f2035e3-16a5-48b0-b4a4-21aae6ede753"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "652f1af2-af77-4129-bfbf-467e7cc390b4",
    "visible": true
}