event_inherited()

if clicked && moveable {
	obj_textbox.charCount = 0
	obj_textbox.page = 1	
}

if !clicked && moveable{
	script_execute(scr_placement)
	
	if x < 1500 && x > 1200 {
		if y < 3400 && y > 3200 {
			if image_angle < 11 || image_angle > 350 {
				audio_play_sound(snd_ding, 2, 0);
				moveable = false;
				x = 1375
				y = 3325
				image_angle = -0.56
				global.peices += 1
			}
		}
	}
}