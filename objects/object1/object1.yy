{
    "id": "c79bfb1c-f621-4a9c-960d-6e9417811f60",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object1",
    "eventList": [
        {
            "id": "f6484347-2c02-47d9-979c-0f2971589549",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c79bfb1c-f621-4a9c-960d-6e9417811f60"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "d384bc72-bc54-47d1-9dff-08894ac4da37",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "75c0d1ca-282c-42ae-8c3b-e6d842f3c95c",
    "visible": true
}