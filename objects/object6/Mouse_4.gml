event_inherited()

if clicked && moveable {
	obj_textbox.charCount = 0
	obj_textbox.page = 4	
}

if !clicked && moveable{
	script_execute(scr_placement)
	
	if x < 2500 && x > 1950 {
		if y < 2430 && y > 2150 {
			if image_angle < 21 || image_angle > 345 {
				audio_play_sound(snd_ding, 2, 0);
				moveable = false;
				x = 2270
				y = 2280
				image_angle = 0
				global.peices += 1
			}
		}
	}
}