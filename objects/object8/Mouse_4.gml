event_inherited()

if clicked && moveable {
	obj_textbox.charCount = 0
	obj_textbox.page = 9	
}

if !clicked && moveable{
	script_execute(scr_placement)
	
	if x < 1450 && x > 1250 {
		if y < 2910 && y > 2710 {
			if image_angle < 10 || image_angle > 350 {
				audio_play_sound(snd_ding, 2, 0);
				moveable = false;
				x = 1350
				y = 2810
				image_angle = 0
				global.peices += 1
			}
		}
	}
}