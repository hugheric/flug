{
    "id": "75db853d-3875-4eff-a28e-f4dfe64d633c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 280,
    "bbox_left": 71,
    "bbox_right": 389,
    "bbox_top": 88,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89061b23-01e5-4d5f-bca4-99eec48a93b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75db853d-3875-4eff-a28e-f4dfe64d633c",
            "compositeImage": {
                "id": "17d7a417-66e4-4f11-b3ba-d50602f2ccca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89061b23-01e5-4d5f-bca4-99eec48a93b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a598113-f3a5-42b4-b863-3018ea8dea46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89061b23-01e5-4d5f-bca4-99eec48a93b2",
                    "LayerId": "8ee2afff-1c37-4e6c-9269-6b173c90a5ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 369,
    "layers": [
        {
            "id": "8ee2afff-1c37-4e6c-9269-6b173c90a5ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75db853d-3875-4eff-a28e-f4dfe64d633c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 449,
    "xorig": 154,
    "yorig": 169
}