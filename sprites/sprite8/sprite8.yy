{
    "id": "7caf7c00-f6ce-4f92-b35f-b90e1e464750",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 387,
    "bbox_left": 66,
    "bbox_right": 230,
    "bbox_top": 49,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b34c4da4-d00a-46b2-8ae4-05156cb77cd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7caf7c00-f6ce-4f92-b35f-b90e1e464750",
            "compositeImage": {
                "id": "ec5cc642-4183-4fe1-8eb7-e6d6d34a8a2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b34c4da4-d00a-46b2-8ae4-05156cb77cd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf642dda-7b37-4449-90e3-22ef92a89694",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b34c4da4-d00a-46b2-8ae4-05156cb77cd1",
                    "LayerId": "ae6079e1-1c92-4d6e-9831-85311cd39371"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 457,
    "layers": [
        {
            "id": "ae6079e1-1c92-4d6e-9831-85311cd39371",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7caf7c00-f6ce-4f92-b35f-b90e1e464750",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 305,
    "xorig": 141,
    "yorig": 246
}