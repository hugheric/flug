{
    "id": "4499f179-72b6-4d5e-b463-3e1203bd29c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_noflug",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87cfe111-427b-467a-9d8e-23b6d20a2445",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4499f179-72b6-4d5e-b463-3e1203bd29c6",
            "compositeImage": {
                "id": "32b8cfde-9246-41da-abe4-06f66aace740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87cfe111-427b-467a-9d8e-23b6d20a2445",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43f2da86-887e-4680-9e59-b270e2c3b7dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87cfe111-427b-467a-9d8e-23b6d20a2445",
                    "LayerId": "e00fe7ef-8fac-4df1-81c8-90127e7c5c75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e00fe7ef-8fac-4df1-81c8-90127e7c5c75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4499f179-72b6-4d5e-b463-3e1203bd29c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 53,
    "yorig": 17
}