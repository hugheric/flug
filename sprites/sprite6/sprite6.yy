{
    "id": "23c4dae9-2655-4912-a147-d5871e993488",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 429,
    "bbox_left": 61,
    "bbox_right": 415,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e12d5c7d-662a-402c-8563-2bb746ad8b69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23c4dae9-2655-4912-a147-d5871e993488",
            "compositeImage": {
                "id": "6f97f3f1-8d08-4cd6-921e-c61f7d2fcd74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e12d5c7d-662a-402c-8563-2bb746ad8b69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5a4f37a-bc2a-418f-888a-cfddeb02219d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e12d5c7d-662a-402c-8563-2bb746ad8b69",
                    "LayerId": "eca460b1-1815-4f09-9e67-af10075622ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 513,
    "layers": [
        {
            "id": "eca460b1-1815-4f09-9e67-af10075622ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23c4dae9-2655-4912-a147-d5871e993488",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 463,
    "xorig": 345,
    "yorig": 244
}