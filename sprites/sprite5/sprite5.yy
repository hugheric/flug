{
    "id": "7ce9ae57-64d9-43bf-b9ca-b47fa843cdf4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 540,
    "bbox_left": 124,
    "bbox_right": 550,
    "bbox_top": 33,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c570cb0f-ac41-41e0-8b92-4c63c722b25b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ce9ae57-64d9-43bf-b9ca-b47fa843cdf4",
            "compositeImage": {
                "id": "122a4779-9775-4b7a-a5b1-961fb5de242e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c570cb0f-ac41-41e0-8b92-4c63c722b25b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ac69097-fc6f-428b-92a9-182065421c8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c570cb0f-ac41-41e0-8b92-4c63c722b25b",
                    "LayerId": "66dc47da-ccb7-4c39-9445-2e723dbeef7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 658,
    "layers": [
        {
            "id": "66dc47da-ccb7-4c39-9445-2e723dbeef7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ce9ae57-64d9-43bf-b9ca-b47fa843cdf4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 705,
    "xorig": 170,
    "yorig": 258
}