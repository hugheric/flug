{
    "id": "39a5ec08-1aaa-495a-b7f8-75034befd555",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_textbox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 517,
    "bbox_left": 0,
    "bbox_right": 623,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de5d4f03-adfd-41de-9ffb-ab0093b89eea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39a5ec08-1aaa-495a-b7f8-75034befd555",
            "compositeImage": {
                "id": "3977e76d-7292-4dba-a1c8-1b191bc2eb9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de5d4f03-adfd-41de-9ffb-ab0093b89eea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0783bc0-97ec-4df9-a171-5ba95d0920e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de5d4f03-adfd-41de-9ffb-ab0093b89eea",
                    "LayerId": "82a07454-fa2e-4d11-9ee4-0ae5d8d272c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 518,
    "layers": [
        {
            "id": "82a07454-fa2e-4d11-9ee4-0ae5d8d272c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39a5ec08-1aaa-495a-b7f8-75034befd555",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 624,
    "xorig": 0,
    "yorig": 0
}