{
    "id": "52bce536-eed1-485b-b40c-446b04984e9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_machine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 919,
    "bbox_left": 0,
    "bbox_right": 1867,
    "bbox_top": 64,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4bba6a85-2149-4e1d-b725-3f299b8d4640",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52bce536-eed1-485b-b40c-446b04984e9d",
            "compositeImage": {
                "id": "962c2c04-80c8-496d-8ba6-94ad532521c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bba6a85-2149-4e1d-b725-3f299b8d4640",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd58452e-b85d-4b7e-9694-f2e0db922765",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bba6a85-2149-4e1d-b725-3f299b8d4640",
                    "LayerId": "3c38c284-acb0-4ebf-8845-466195bb6e10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 953,
    "layers": [
        {
            "id": "3c38c284-acb0-4ebf-8845-466195bb6e10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52bce536-eed1-485b-b40c-446b04984e9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1921,
    "xorig": 849,
    "yorig": 478
}