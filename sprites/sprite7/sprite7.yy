{
    "id": "28288e55-a071-40f4-9601-3a14f2fbd322",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 32,
    "bbox_right": 566,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "143b257a-4c72-470e-a565-5718dbda910e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28288e55-a071-40f4-9601-3a14f2fbd322",
            "compositeImage": {
                "id": "172f654a-8eef-4578-b47b-d8d869359864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "143b257a-4c72-470e-a565-5718dbda910e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d6ca68a-033b-429d-8b7e-248396cdaa50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "143b257a-4c72-470e-a565-5718dbda910e",
                    "LayerId": "b0913196-03fc-4866-ace7-67c9f6408fd5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 289,
    "layers": [
        {
            "id": "b0913196-03fc-4866-ace7-67c9f6408fd5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28288e55-a071-40f4-9601-3a14f2fbd322",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 617,
    "xorig": 308,
    "yorig": 144
}