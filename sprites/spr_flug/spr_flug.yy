{
    "id": "4f3ecc62-5c3b-43b7-b4ea-fa018cd2edbe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flug",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24e37de7-ddfa-4c6c-b858-ce164d633743",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f3ecc62-5c3b-43b7-b4ea-fa018cd2edbe",
            "compositeImage": {
                "id": "2d93a660-0c3c-4167-a5f9-295ae0e480ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24e37de7-ddfa-4c6c-b858-ce164d633743",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a7f6ff1-6003-46c9-9cd1-25f828e287ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24e37de7-ddfa-4c6c-b858-ce164d633743",
                    "LayerId": "6a4e602f-7ab5-4f92-954f-45e3d7dd5de7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6a4e602f-7ab5-4f92-954f-45e3d7dd5de7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f3ecc62-5c3b-43b7-b4ea-fa018cd2edbe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}