{
    "id": "ee3be5b7-0ae0-49cc-813b-1e75adce260a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1499,
    "bbox_left": 0,
    "bbox_right": 2099,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e5cd4db5-fd75-46b6-87d0-76f1fdfa951b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee3be5b7-0ae0-49cc-813b-1e75adce260a",
            "compositeImage": {
                "id": "a7db2da3-bb09-4242-a48c-a78952a56f3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5cd4db5-fd75-46b6-87d0-76f1fdfa951b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb95241b-3010-48a7-be2c-61d668e72032",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5cd4db5-fd75-46b6-87d0-76f1fdfa951b",
                    "LayerId": "5d1603b3-2e4b-4721-9a65-0fda3cbdbe09"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1500,
    "layers": [
        {
            "id": "5d1603b3-2e4b-4721-9a65-0fda3cbdbe09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee3be5b7-0ae0-49cc-813b-1e75adce260a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2100,
    "xorig": 0,
    "yorig": 0
}