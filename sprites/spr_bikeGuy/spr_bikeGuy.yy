{
    "id": "044a136e-d344-4dda-80bd-4e0602822d50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bikeGuy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1222,
    "bbox_left": 0,
    "bbox_right": 1875,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1bdbddf3-f11c-4761-b8e2-a47d86f08290",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "044a136e-d344-4dda-80bd-4e0602822d50",
            "compositeImage": {
                "id": "04636e0e-50de-4f74-86c3-7258d391e100",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bdbddf3-f11c-4761-b8e2-a47d86f08290",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e24e4f9-b479-4093-b50c-2c00edc7cdee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bdbddf3-f11c-4761-b8e2-a47d86f08290",
                    "LayerId": "cce0e772-4630-465a-bd6c-c92608ce3203"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1542,
    "layers": [
        {
            "id": "cce0e772-4630-465a-bd6c-c92608ce3203",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "044a136e-d344-4dda-80bd-4e0602822d50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2115,
    "xorig": 0,
    "yorig": 0
}