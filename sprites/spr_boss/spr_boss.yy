{
    "id": "5fee2569-ab4b-453d-9b87-8938a1e16b77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2183,
    "bbox_left": 29,
    "bbox_right": 3559,
    "bbox_top": 50,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21997d24-0669-4917-84ea-2aa3d4b73cb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fee2569-ab4b-453d-9b87-8938a1e16b77",
            "compositeImage": {
                "id": "d03189b4-5b6d-42e1-87c8-214ac4e4cdfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21997d24-0669-4917-84ea-2aa3d4b73cb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e912280d-44b9-458b-93d4-f1eb5afe1d00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21997d24-0669-4917-84ea-2aa3d4b73cb3",
                    "LayerId": "5fe37fa1-0f92-444c-9d44-6bc53d63a0cb"
                }
            ]
        },
        {
            "id": "247fd578-3059-42b4-ba9f-1326646c2d1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fee2569-ab4b-453d-9b87-8938a1e16b77",
            "compositeImage": {
                "id": "60de1b51-f6cb-4323-a013-34581a5cd5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "247fd578-3059-42b4-ba9f-1326646c2d1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a569192-033f-4f39-8a41-f2a94c1e3205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "247fd578-3059-42b4-ba9f-1326646c2d1c",
                    "LayerId": "5fe37fa1-0f92-444c-9d44-6bc53d63a0cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2239,
    "layers": [
        {
            "id": "5fe37fa1-0f92-444c-9d44-6bc53d63a0cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5fee2569-ab4b-453d-9b87-8938a1e16b77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3585,
    "xorig": 0,
    "yorig": 0
}