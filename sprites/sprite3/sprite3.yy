{
    "id": "7328bec5-b59c-49bb-9e9d-954fdaeaac69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 463,
    "bbox_left": 52,
    "bbox_right": 602,
    "bbox_top": 48,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31d0fb46-0a05-4340-8ca8-1c1e5f148f72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7328bec5-b59c-49bb-9e9d-954fdaeaac69",
            "compositeImage": {
                "id": "dfa461f3-9244-45bb-8eb8-d89a45fa9eb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31d0fb46-0a05-4340-8ca8-1c1e5f148f72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "240eaea1-7b7e-4d5f-9f22-fd901f757395",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31d0fb46-0a05-4340-8ca8-1c1e5f148f72",
                    "LayerId": "698aed7f-0eae-4eaa-bd79-e89b4bba9e56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 489,
    "layers": [
        {
            "id": "698aed7f-0eae-4eaa-bd79-e89b4bba9e56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7328bec5-b59c-49bb-9e9d-954fdaeaac69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 673,
    "xorig": 316,
    "yorig": 256
}