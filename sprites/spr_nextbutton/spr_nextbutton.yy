{
    "id": "652f1af2-af77-4129-bfbf-467e7cc390b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nextbutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 0,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5534177e-b5ec-43ed-8e91-6e701931aecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "652f1af2-af77-4129-bfbf-467e7cc390b4",
            "compositeImage": {
                "id": "25f3d29a-1f56-4fa1-982a-4ca68b8fc3a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5534177e-b5ec-43ed-8e91-6e701931aecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b531cf3d-76a7-4df4-8650-f3f944ca1fae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5534177e-b5ec-43ed-8e91-6e701931aecc",
                    "LayerId": "40aa7740-f1fe-4da2-bae4-7a8ea92dec65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "40aa7740-f1fe-4da2-bae4-7a8ea92dec65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "652f1af2-af77-4129-bfbf-467e7cc390b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}