{
    "id": "af5475dc-478c-4330-a4d9-e97e0b7cd0f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_end",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1670,
    "bbox_left": 0,
    "bbox_right": 1978,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f198803-0ffd-468c-a5e1-2124918f9ce7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af5475dc-478c-4330-a4d9-e97e0b7cd0f1",
            "compositeImage": {
                "id": "d3a8e0b2-44fd-43f2-b4da-e0388dc9b8de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f198803-0ffd-468c-a5e1-2124918f9ce7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5c9f3c6-9521-4338-aa70-580ed44b14a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f198803-0ffd-468c-a5e1-2124918f9ce7",
                    "LayerId": "86fb8f03-43a0-427c-9b65-1118ff835b6a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1671,
    "layers": [
        {
            "id": "86fb8f03-43a0-427c-9b65-1118ff835b6a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af5475dc-478c-4330-a4d9-e97e0b7cd0f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1979,
    "xorig": 0,
    "yorig": 0
}