{
    "id": "26933fd2-7455-4e7e-a29e-05ffb1d0d25c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blueprint",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1171,
    "bbox_left": 0,
    "bbox_right": 1611,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "579cb26e-bcba-4c1f-815e-d7f2e3b26d8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26933fd2-7455-4e7e-a29e-05ffb1d0d25c",
            "compositeImage": {
                "id": "768a174c-1dd5-4fd4-887e-a4571f7f4eae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "579cb26e-bcba-4c1f-815e-d7f2e3b26d8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0ddc790-2c4a-4522-ac20-1cfa416f3511",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "579cb26e-bcba-4c1f-815e-d7f2e3b26d8a",
                    "LayerId": "21ace4fa-bbac-4545-af16-8b9649dfc9e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1172,
    "layers": [
        {
            "id": "21ace4fa-bbac-4545-af16-8b9649dfc9e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26933fd2-7455-4e7e-a29e-05ffb1d0d25c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1612,
    "xorig": 0,
    "yorig": 0
}