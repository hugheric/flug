{
    "id": "e9de17ff-e8d9-4af5-9621-acd019b79f02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_info",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 517,
    "bbox_left": 0,
    "bbox_right": 623,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "812414cf-e228-46ce-8a08-9bd0884354f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9de17ff-e8d9-4af5-9621-acd019b79f02",
            "compositeImage": {
                "id": "332802ac-38e7-4dba-8930-24f24de2b976",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "812414cf-e228-46ce-8a08-9bd0884354f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9777e1a-381e-497d-86fd-c01f97f9fe20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "812414cf-e228-46ce-8a08-9bd0884354f6",
                    "LayerId": "3c969f6b-7d93-4a1a-9ee4-3713698859a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 518,
    "layers": [
        {
            "id": "3c969f6b-7d93-4a1a-9ee4-3713698859a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9de17ff-e8d9-4af5-9621-acd019b79f02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 624,
    "xorig": 196,
    "yorig": 96
}