{
    "id": "75c0d1ca-282c-42ae-8c3b-e6d842f3c95c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 597,
    "bbox_left": 107,
    "bbox_right": 819,
    "bbox_top": 50,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71b83a25-b64e-4de1-9b01-ddc8f289f5e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75c0d1ca-282c-42ae-8c3b-e6d842f3c95c",
            "compositeImage": {
                "id": "cb3cfbcb-112f-48ea-9607-26002b8c76f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71b83a25-b64e-4de1-9b01-ddc8f289f5e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85366f69-3f73-4a90-b21c-1b27724276cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b83a25-b64e-4de1-9b01-ddc8f289f5e5",
                    "LayerId": "8672bd39-0977-48bf-9ead-6f5014d9c4d1"
                },
                {
                    "id": "6a1c39a2-9782-434f-a6a7-a6a83828f4bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71b83a25-b64e-4de1-9b01-ddc8f289f5e5",
                    "LayerId": "e7830d7e-7414-47e6-9caa-8d44c24becd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 649,
    "layers": [
        {
            "id": "e7830d7e-7414-47e6-9caa-8d44c24becd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75c0d1ca-282c-42ae-8c3b-e6d842f3c95c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "8672bd39-0977-48bf-9ead-6f5014d9c4d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75c0d1ca-282c-42ae-8c3b-e6d842f3c95c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 913,
    "xorig": 449,
    "yorig": 319
}